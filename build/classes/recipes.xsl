<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:output
        method="html"
        doctype-system="about:legacy-compat"
        encoding="UTF-8"
        indent="yes"  
        omit-xml-declaration="yes" />
    <xsl:template match="/">
        <html>
            <head>
                <title>Web Data TMA 2016</title>
                <link rel="stylesheet" href="recipes.css" />
            </head>
            <body>
                <section>
                <xsl:for-each select="bbc_recipes/recipe"> 
                    <xsl:sort select="name"/>
                    <div class="paragraph">
                        <h3><xsl:value-of select="name"/></h3>
                        <p>Preparation time: <xsl:value-of select="prep_time"/> mins.
                        </p>
                        <p>Cooking time: <xsl:value-of select="cooking_time"/> mins. 
                            <xsl:choose>
                                <xsl:when test="cooking_time &gt; 60">Slow Burner. </xsl:when>
                                <xsl:when test="cooking_time &lt; 30">Quick and Easy. </xsl:when>
                                <xsl:otherwise>Medium Burner. </xsl:otherwise>
                            </xsl:choose>
                        </p>
                        <p>Serves: <xsl:value-of select="quantity"/>
                        </p>
                        <p>Difficulty: <xsl:value-of select="difficult"/>
                        </p>
                        <p>Overview: <xsl:value-of select="heading"/>
                        </p>
                        <h4>Ingredients: </h4>
                        <ul>
                            <xsl:for-each select="ingredient">
                            <li>
				<xsl:value-of select="."/>
                             </li>
                            </xsl:for-each>
                        </ul>
                    </div>
                </xsl:for-each>
                </section>
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
